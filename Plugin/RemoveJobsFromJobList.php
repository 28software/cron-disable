<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

namespace TwentyEightSoftware\CronDisable\Plugin;

use Magento\Cron\Model\ConfigInterface;
use TwentyEightSoftware\CronDisable\Model\ConfigProvider;

/**
 * Class RemoveJobsFromJobList
 */
class RemoveJobsFromJobList
{
    /**
     * @var ConfigProvider
     */
    private $configProvider;

    /**
     * RemoveJobsFromJobList constructor.
     *
     * @param ConfigProvider $configProvider
     */
    public function __construct(
        ConfigProvider $configProvider
    ) {
        $this->configProvider = $configProvider;
    }

    /**
     * @param ConfigInterface $subject
     * @param array           $result
     *
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterGetJobs(ConfigInterface $subject, $result): array
    {
        $disabledCronJobs = $this->configProvider->getDisabledJobs();
        foreach ($result as $groupId => $jobList) {
            foreach (array_keys($jobList) as $jobCode) {
                if (in_array($jobCode, $disabledCronJobs, true)) {
                    unset($result[$groupId][$jobCode]);
                }
            }
        }

        return $result;
    }
}

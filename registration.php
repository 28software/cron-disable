<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

use \Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'TwentyEightSoftware_CronDisable', __DIR__);

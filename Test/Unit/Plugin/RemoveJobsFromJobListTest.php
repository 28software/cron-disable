<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

namespace TwentyEightSoftware\CronDisable\Test\Unit\Plugin;

use TwentyEightSoftware\CronDisable\Model\ConfigProvider;
use TwentyEightSoftware\CronDisable\Plugin\RemoveJobsFromJobList;
use Magento\Cron\Model\ConfigInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use PHPUnit\Framework\MockObject\MockObject as Mock;
use PHPUnit\Framework\TestCase;

/**
 * Class RemoveJobsFromJobListTest
 */
class RemoveJobsFromJobListTest extends TestCase
{
    /**
     * @var RemoveJobsFromJobList
     */
    private $testObject;

    /**
     * @var ScopeConfigInterface|Mock
     */
    private $scopeConfigMock;

    /**
     * @var ConfigInterface|Mock
     */
    private $configMock;

    /**
     * @var ConfigProvider|Mock
     */
    private $configProviderMock;

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        parent::setUp();

        $this->scopeConfigMock = $this->createPartialMock(
            ScopeConfigInterface::class,
            ['getValue', 'isSetFlag']
        );
        $this->configMock = $this->createPartialMock(
            ConfigInterface::class,
            ['getJobs']
        );
        $this->configProviderMock = $this->createPartialMock(
            ConfigProvider::class,
            ['getDisabledJobs']
        );

        $this->testObject = new RemoveJobsFromJobList(
            $this->configProviderMock
        );
    }

    /**
     * Test afterGetJobs
     *
     * @dataProvider dataProvider()
     */
    public function testAfterGetJobs($actualJobsList, $jobsToRemove, $result)
    {
        $this->configProviderMock->expects(self::once())
            ->method('getDisabledJobs')
            ->willReturn($jobsToRemove);

        self::assertEquals(
            $result,
            $this->testObject->afterGetJobs($this->configMock, $actualJobsList)
        );
    }

    /**
     * Data provider
     *
     * @return array
     */
    public function dataProvider(): array
    {
        return [
            [
                //Jobs actual
                [
                    'default' =>
                        [
                            'test1' => 'test_1',
                            'test2' => 'test_2',
                            'test3' => 'test_3'
                        ]
                ],
                //Jobs to remove
                [
                    'test2'
                ],
                //Expected result
                [
                    'default' =>
                        [
                            'test1' => 'test_1',
                            'test3' => 'test_3'
                        ]
                ]

            ]
        ];
    }
}

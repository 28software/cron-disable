<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

namespace TwentyEightSoftware\CronDisable\Test\Unit\Model;

use TwentyEightSoftware\CronDisable\Model\ConfigProvider;
use Magento\Framework\App\Config\ScopeConfigInterface;
use PHPUnit\Framework\MockObject\MockObject as Mock;
use PHPUnit\Framework\TestCase;

/**
 * Class ConfigProviderTest
 */
class ConfigProviderTest extends TestCase
{
    /**
     * @var ConfigProvider
     */
    private $model;

    /**
     * @var ScopeConfigInterface|Mock
     */
    private $scopeConfigMock;

    protected function setUp()
    {
        parent::setUp();

        $this->scopeConfigMock = $this->createMock(ScopeConfigInterface::class);

        $this->model = new ConfigProvider($this->scopeConfigMock);
    }

    /**
     * Test getDisabledJobs
     *
     * @dataProvider dataProvider
     */
    public function testGetDisabledJobs($configValue, $result)
    {
        $this->scopeConfigMock->expects(self::once())
            ->method('getValue')
            ->with(ConfigProvider::XML_CRON_DISABLE_JOBS)
            ->willReturn($configValue);

        $this->assertArraySubset($this->model->getDisabledJobs(), $result);
    }

    public function dataProvider()
    {
        return [
            [
                'test1,test2',
                [
                    'test1',
                    'test2'
                ]
            ],
            [
                'test3',
                [
                    'test3'
                ]
            ],
            [
                '',
                []
            ]
        ];
    }
}

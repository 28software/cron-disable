<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

namespace TwentyEightSoftware\CronDisable\Test\Unit\Model\Config\Source;

use Magento\Cron\Model\ConfigInterface;
use Magento\Developer\Model\Config\Source\WorkflowType;
use Magento\Framework\Data\OptionSourceInterface;
use PHPUnit\Framework\MockObject\MockObject as Mock;
use PHPUnit\Framework\TestCase;
use TwentyEightSoftware\CronDisable\Model\Config\Source\Job;

/**
 * Class JobTest
 */
class JobTest extends TestCase
{
    /**
     * @var WorkflowType
     */
    protected $model;

    /**
     * @var Mock|ConfigInterface
     */
    private $configMock;

    /**
     * Test toOptionArray
     */
    public function testToOptionArray()
    {
        $this->assertInstanceOf(OptionSourceInterface::class, $this->model);
        $this->assertCount(5, $this->model->toOptionArray());
    }

    /**
     * Test option structure
     */
    public function testOptionStructure()
    {
        foreach ($this->model->toOptionArray() as $option) {
            $this->assertArrayHasKey('value', $option);
            $this->assertArrayHasKey('label', $option);
        }
    }

    /**
     * @inheritDoc
     */
    protected function setUp()
    {
        $jobs = [
            'default' =>
                [
                    'test1' => 'test1',
                    'test2' => 'test2',
                    'test3' => 'test3',
                ],
            'indexer' =>
                [
                    'test4' => 'test4',
                    'test5' => 'test6',
                ],
        ];

        $this->configMock = $this->createPartialMock(
            ConfigInterface::class,
            ['getJobs']
        );
        $this->configMock
            ->expects(self::once())
            ->method('getJobs')
            ->willReturn($jobs);

        $this->model = new Job($this->configMock);
    }
}

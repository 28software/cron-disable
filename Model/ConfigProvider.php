<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

namespace TwentyEightSoftware\CronDisable\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class ConfigProvider
 */
class ConfigProvider
{
    /**
     * Disabled cron jobs list xml path
     */
    public const XML_CRON_DISABLE_JOBS = 'system/cron/disable_jobs';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * ConfigProvider constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * Get disabled cron jobs list
     *
     * @return array
     */
    public function getDisabledJobs(): array
    {
        $disabledCronJobs = $this->scopeConfig->getValue(self::XML_CRON_DISABLE_JOBS);

        return $disabledCronJobs ? explode(',', $disabledCronJobs) : [];
    }
}

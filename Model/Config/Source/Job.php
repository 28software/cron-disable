<?php
/**
 * Copyright © 28Software, LLC. All rights reserved.
 */
declare(strict_types = 1);

namespace TwentyEightSoftware\CronDisable\Model\Config\Source;

use Magento\Cron\Model\ConfigInterface;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Job
 */
class Job implements OptionSourceInterface
{
    /**
     * @var ConfigInterface
     */
    private $config;

    /**
     * Job constructor.
     *
     * @param ConfigInterface $config
     */
    public function __construct(ConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    public function toOptionArray(): array
    {
        $result = [];
        $jobs = $this->config->getJobs();
        /** @var string $groupId */
        /** @var array $jobsRoot */
        foreach ($jobs as $jobsRoot) {
            foreach (array_keys($jobsRoot) as $jobCode) {
                $result[] = [
                    'value' => $jobCode,
                    'label' => $jobCode,
                ];
            }
        }

        return $result;
    }
}
